package sample;

import java.util.ArrayList;

class TemplatesBuilder {
    private String rawText;
    private ArrayList<String> templateBlocks;
    private Integer stringsInBlockCount;
    private Integer titleStringNumber;
    private Integer featuringStringNumber;
    private Integer tagsStringNumber;
    private String title;
    private String featuring;
    private String tags;

    /**
     * Constructor
     * @param stringsInBlockCount Integer
     * @param titleStringNumber Integer
     * @param featuringStringNumber Integer
     * @param tagsStringNumber Integer
     */
    TemplatesBuilder(String rawText, Integer stringsInBlockCount, Integer titleStringNumber, Integer featuringStringNumber, Integer tagsStringNumber) {
        this.templateBlocks = new ArrayList<>();
        this.resetStrings();
        this.rawText = rawText;
        this.stringsInBlockCount = stringsInBlockCount;
        this.titleStringNumber = titleStringNumber;
        this.featuringStringNumber = featuringStringNumber;
        this.tagsStringNumber = tagsStringNumber;
    }

    /**
     * reset strings
     */
    private void resetStrings() {
        this.title = "";
        this.featuring = "";
        this.tags = "";
    }

    /**
     * get template blocks
     * @return ArrayList<Strings>
     */
    ArrayList<String> getTemplateBlocks() {
        int i = 1;
        String[] lines = this.rawText.split("\\n");
        for (String line: lines) {
            if (i == 1) {
                this.resetStrings();
            }
            if (this.titleStringNumber == i) {
                this.title = line;
            }
            if (this.featuringStringNumber == i) {
                this.featuring = line;
            }
            if (this.tagsStringNumber == i) {
                this.tags = line;
            }
            if (i < stringsInBlockCount) {
                i++;
            } else {
                // fill template
            Template template = new Template(this.title, this.featuring, this.tags);
                this.templateBlocks.add(template.toString());
                i = 1;
            }
        }
        return this.templateBlocks;
    }
}
