package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene mainScene = new Scene(root, 1000, 600);
        primaryStage.setTitle("XXXEditor v2.2.0");
        primaryStage.setScene(mainScene);
        primaryStage.setMinWidth(720);
        primaryStage.setMinHeight(400);
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream( "icon.png" )));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
