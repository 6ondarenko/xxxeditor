package sample;

// import javafx.application.Platform;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    public static final String POSTS_SEPARATOR = "=======";

    @FXML
    CheckBox boldSubHeadersCheckbox;

    @FXML
    AnchorPane mainAnchorPane;

    @FXML
    private TextField countTextField;

    @FXML
    private TextField countSymbolsInCounterTextField;

    @FXML
    private TextArea preformattedTextArea;

    @FXML
    private TextArea postformattedTextArea;

    @FXML
    private TextArea preformattedDoubleTextArea;

    @FXML
    private TextArea postformattedDoubleTextArea;

    @FXML
    private TextArea parser2PreformattedTextArea;

    @FXML
    private TextArea parser2PostformattedTextArea;

    @FXML
    private TextField parser2Text;

    @FXML
    private TextArea preformattedMixTextArea;

    @FXML
    private TextArea postformattedMixTextArea;

    @FXML
    private Button formatButton;

    @FXML
    private Button cleanButton;

    @FXML
    private Button resultsCopyButton;


    @FXML
    private Button formatButtonDouble;

    @FXML
    private Button cleanButtonDouble;

    @FXML
    private Button resultsCopyButtonDouble;

    @FXML
    private Button parser2FormatButton;

    @FXML
    private Button parser2CleanButton;

    @FXML
    private Button parser2ResultsCopyButton;

    @FXML
    private Button mixButton;

    @FXML
    private Button mixCleanButton;

    @FXML
    private Button mixResultsCopyButton;

    @FXML
    private TextField totalStringsInBlockCount;

    @FXML
    private TextField titleStringNumber;

    @FXML
    private TextField featuringStringNumber;

    @FXML
    private TextField tagsStringNumber;

    @FXML
    private ImageView fullScreenButtonOnTab1;

    @FXML
    private ImageView fullScreenButtonOffTab1;

    @FXML
    private ImageView fullScreenButtonOnTab2;

    @FXML
    private ImageView fullScreenButtonOffTab2;

    @FXML
    private ImageView fullScreenButtonOnTab3;

    @FXML
    private ImageView fullScreenButtonOffTab3;

    @FXML
    private ImageView fullScreenButtonOnTab4;

    @FXML
    private ImageView fullScreenButtonOffTab4;

    /**
     * String post start marker
     */
    private static final String POST_START_MARKER = "=======";

    /**
     * Constructor
     */
    public Controller() {
    }

    /**
     * number validate method
     */
    private void validateNumberOrEmptyTextField(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[0-9]*")) {
                textField.setText(oldValue);
            }
        });
    }

    /**
     * remove empty lines and redundant whitespaces
     *
     * @param text String
     * @return String
     */
    private String cleanText(String text) {
        return text.replaceAll("(?m)^[ \\t]*\\r?\\n", "")
                .replaceAll(" {2,3}", " ")
                .replaceAll(" {3,}", ", ")
                .replaceAll("(?m) +$", "");
    }

    /**
     * add text areas listeners
     */
    private void addTextAreasListeners() {
        preformattedTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            cleanButton.setDisable(newValue.isEmpty());
            formatButton.setDisable(newValue.isEmpty());
        });
        postformattedTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            cleanButton.setDisable(newValue.isEmpty());
            resultsCopyButton.setDisable(newValue.isEmpty());
        });
        parser2PreformattedTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            parser2CleanButton.setDisable(newValue.isEmpty());
            parser2FormatButton.setDisable(newValue.isEmpty());
        });
        parser2PostformattedTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            parser2CleanButton.setDisable(newValue.isEmpty());
            parser2ResultsCopyButton.setDisable(newValue.isEmpty());
        });
        preformattedMixTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            mixCleanButton.setDisable(newValue.isEmpty());
            mixButton.setDisable(newValue.isEmpty());
        });
        postformattedMixTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            mixCleanButton.setDisable(newValue.isEmpty());
            mixResultsCopyButton.setDisable(newValue.isEmpty());
        });
        preformattedDoubleTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            cleanButtonDouble.setDisable(newValue.isEmpty());
            formatButtonDouble.setDisable(newValue.isEmpty());
        });
        postformattedDoubleTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            cleanButtonDouble.setDisable(newValue.isEmpty());
            resultsCopyButtonDouble.setDisable(newValue.isEmpty());
        });
    }

    /**
     * validate text fields
     */
    private void validateTextFields() {
        validateNumberOrEmptyTextField(countTextField);
        validateNumberOrEmptyTextField(countSymbolsInCounterTextField);
        validateNumberOrEmptyTextField(totalStringsInBlockCount);
        validateNumberOrEmptyTextField(titleStringNumber);
        validateNumberOrEmptyTextField(featuringStringNumber);
        validateNumberOrEmptyTextField(tagsStringNumber);
    }

    /**
     * set draggable text area
     *
     * @param textArea TextArea
     */
    private void setDraggableTextArea(TextArea textArea) {
        textArea.setOnDragOver((dragEvent) -> {
            Dragboard dragboard = dragEvent.getDragboard();
            dragEvent.acceptTransferModes(TransferMode.COPY);
            if (!dragboard.hasFiles() || !dragEvent.isAccepted()) {
                return;
            }
            textArea.setOnDragDropped((dragDoneEvent) -> {
                for (File file : dragboard.getFiles()) {
                    String absolutePath = file.getAbsolutePath();
                    try {
                        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(absolutePath)));
                        StringBuilder stringBuilder = new StringBuilder();
                        String nl = System.getProperty("line.separator", "\n");
                        String line;
                        while ((line = br.readLine()) != null) {
                            stringBuilder.append(line).append(nl);
                        }
                        textArea.setText(stringBuilder.toString().trim());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            });
        });
    }

    @FXML
    private void initialize() {
        // add textareas listeners
        this.addTextAreasListeners();
        // validate text fields
        this.validateTextFields();
        // make preformatted text draggable
        this.setDraggableTextArea(preformattedTextArea);
        // make preformatted mix text draggable
        this.setDraggableTextArea(preformattedMixTextArea);
        // make preformatted text in parser 2 draggable
        this.setDraggableTextArea(parser2PreformattedTextArea);
        // make preformatted text in double target draggable
        this.setDraggableTextArea(preformattedDoubleTextArea);
    }

    /**
     * get count strings of one block
     *
     * @return Integer
     */
    private Integer getStringsInBlockCount() {
        return this.totalStringsInBlockCount.getText().isEmpty() ? 1 : Integer.parseInt(totalStringsInBlockCount.getText());
    }

    /**
     * get counter value
     *
     * @return Integer
     */
    private Integer getCounter() {
        return countTextField.getText().isEmpty() ? 1 : Integer.parseInt(countTextField.getText());
    }

    /**
     * get symbols count of counter
     *
     * @return Integer
     */
    private Integer getCounterSymbolsCount() {
        return countSymbolsInCounterTextField.getText().isEmpty() ? 3 : Integer.parseInt(countSymbolsInCounterTextField.getText());
    }

    /**
     * get number of title string in raw text
     *
     * @return Integer
     */
    private Integer getTitleStringNumber() {
        return titleStringNumber.getText().isEmpty() ? 0 : Integer.parseInt(titleStringNumber.getText());
    }

    /**
     * get number of featuring string in raw text
     *
     * @return Integer
     */
    private Integer getFeaturingStringNumber() {
        return featuringStringNumber.getText().isEmpty() ? 0 : Integer.parseInt(featuringStringNumber.getText());
    }

    /**
     * get number of tags string in raw text
     *
     * @return Integer
     */
    private Integer getTagsStringNumber() {
        return tagsStringNumber.getText().isEmpty() ? 0 : Integer.parseInt(tagsStringNumber.getText());
    }

    /**
     * fetch template blocks
     *
     * @return ArrayList<String>
     */
    private ArrayList<String> fetchTemplateBlocks() {
        String preformattedText = this.cleanText(preformattedTextArea.getText());
        TemplatesBuilder templatesBuilder = new TemplatesBuilder(
                preformattedText,
                this.getStringsInBlockCount(),
                this.getTitleStringNumber(),
                this.getFeaturingStringNumber(),
                this.getTagsStringNumber()
        );
        return templatesBuilder.getTemplateBlocks();
    }

    /**
     * check text areas
     *
     * @return boolean
     */
    private boolean checkTextareas() {
        return postformattedTextArea.getText().isEmpty() | postformattedTextArea.getText().isEmpty();
    }

    /**
     * check text areas
     *
     * @return boolean
     */
    private boolean checkTextareasDouble() {
        return postformattedDoubleTextArea.getText().isEmpty() | postformattedDoubleTextArea.getText().isEmpty();
    }

    /**
     * fill postformatted text area by template blocks
     */
    private void fillPostformattedTextField() {
        StringBuilder postFormattedText = new StringBuilder();
        ArrayList<String> templateBlocks = fetchTemplateBlocks();
        int counter = this.getCounter();
        int counterSymbolsCount = this.getCounterSymbolsCount();
        String stringCounter;
        for (String templateBlock : templateBlocks) {
            if (counterSymbolsCount > 0) {
                stringCounter = StringUtils.leftPad(Integer.toString(counter), counterSymbolsCount, '0');
            } else {
                stringCounter = Integer.toString(counter);
            }
            postFormattedText.append(stringCounter);
            postFormattedText.append("\n");
            postFormattedText.append(templateBlock);
            counter++;
        }
        // set text of post formatted text area
        postformattedTextArea.setText(postFormattedText.toString());
        // check postformattedTextArea content for setting copy button activity
        resultsCopyButton.setDisable(postformattedTextArea.getText().isEmpty());
        // check textareas content for stetting clean button activity
        cleanButton.setDisable(this.checkTextareas());
    }

    /**
     * fill postformatted text area by template blocks
     */
    private void fillPostformattedDoubleTextField() {
        String preformattedText = preformattedDoubleTextArea.getText();
        postformattedDoubleTextArea.setText(cleanText(preformattedText));

        String[] preformattedArray = preformattedText.split(POSTS_SEPARATOR);
        ArrayList<String> preformattedArrayList = new ArrayList<>(Arrays.asList(preformattedArray));
        StringBuilder postformattedString = new StringBuilder();

        preformattedArrayList.forEach(post -> {
            String postClean = post
                    .replaceAll(POSTS_SEPARATOR, "")
                    .replaceAll("(?m)^\\n+ *", "")
                    .replaceAll("(?m)^ +", "")
                    .trim();
            if (postClean.isEmpty()) return;

            String[] postLines = postClean.split("\\n");

            if (postLines.length < 10) {
                postformattedString.append("Неверный формат поста!").append("\n").append(POSTS_SEPARATOR).append("\n");
                return;
            }

            postformattedString.append(postLines[0]).append("\n");
            postformattedString.append(postLines[1]).append("\n");
            postformattedString.append(postLines[2]).append("\n");
            postformattedString.append(postLines[3]).append("\n");
            postformattedString.append(postLines[4]).append("\n");
            postformattedString.append(postLines[0]).append("\n");
            postformattedString.append(postLines[1]).append("\n");
            postformattedString.append(postLines[5]).append("\n");
            postformattedString.append(postLines[6]).append("\n");
            postformattedString.append(postLines[7]).append("\n");
            postformattedString.append(postLines[8]).append("\n");
            postformattedString.append(postLines[9]).append("\n").append(POSTS_SEPARATOR).append("\n");
        });
        String output = postformattedString.toString();
        postformattedDoubleTextArea.setText(output);
    }

    /**
     * clear text areas and disable buttons
     *
     * @param preformattedTextArea  TextArea
     * @param postformattedTextArea TextArea
     * @param mainButton            Button
     * @param cleanButton           Button
     * @param copyButton            Button
     */
    private void resetForm(TextArea preformattedTextArea, TextArea postformattedTextArea, Button mainButton, Button cleanButton, Button copyButton) {
        preformattedTextArea.setText("");
        postformattedTextArea.setText("");
        mainButton.setDisable(true);
        cleanButton.setDisable(true);
        copyButton.setDisable(true);
    }

    @FXML
    private void handleFormatButtonAction() {
        this.fillPostformattedTextField();
    }


    @FXML
    private void handleFormatButtonDoubleAction() {
        this.fillPostformattedDoubleTextField();
    }

    @FXML
    private void handleMixButtonAction() {
        if (!preformattedMixTextArea.getText().isEmpty()) {
            StringBuilder postformattedText = new StringBuilder();
            ArrayList<String> postsList = new ArrayList<>();
            String preformattedText = this.cleanText(preformattedMixTextArea.getText());
            Collections.addAll(postsList, preformattedText.split(Controller.POST_START_MARKER));
            Collections.shuffle(postsList);
            postsList.removeAll(Arrays.asList("", null));
            for (String postString : postsList) {
                postformattedText.append("\n");
                postformattedText.append(Controller.POST_START_MARKER);
                postformattedText.append("\n");
                postformattedText.append(postString);
            }
            String cleanPostformattedText = this.cleanText(postformattedText.toString());
            postformattedMixTextArea.setText(cleanPostformattedText);
        }
    }

    @FXML
    private void handleMixResultsCopyButtonAction() {
        StringSelection stringSelection = new StringSelection(postformattedMixTextArea.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
    }

    @FXML
    private void handleResultsCopyButtonAction() {
        StringSelection stringSelection = new StringSelection(postformattedTextArea.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
    }

    @FXML
    private void handleResultsCopyButtonDubleAction() {
        StringSelection stringSelection = new StringSelection(postformattedDoubleTextArea.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
    }

    @FXML
    private void handleMixCleanButtonAction() {
        this.resetForm(preformattedMixTextArea, postformattedMixTextArea, mixButton, mixCleanButton, mixResultsCopyButton);
    }

    @FXML
    private void handleCleanButtonAction() {
        this.resetForm(preformattedTextArea, postformattedTextArea, formatButton, cleanButton, resultsCopyButton);
    }

    @FXML
    private void handleCleanButtonDubleAction() {
        this.resetForm(preformattedDoubleTextArea, postformattedDoubleTextArea, formatButtonDouble, cleanButtonDouble, resultsCopyButtonDouble);
    }

    @FXML
    private void handleFullScreenClickOn() {
        Stage primaryStage = (Stage) fullScreenButtonOnTab1.getScene().getWindow();
        primaryStage.setFullScreen(true);
        fullScreenButtonOnTab1.setVisible(false);
        fullScreenButtonOffTab1.setVisible(true);
        fullScreenButtonOnTab2.setVisible(false);
        fullScreenButtonOffTab2.setVisible(true);
        fullScreenButtonOnTab3.setVisible(false);
        fullScreenButtonOffTab3.setVisible(true);
        fullScreenButtonOnTab4.setVisible(false);
        fullScreenButtonOffTab4.setVisible(true);
        primaryStage.fullScreenProperty().addListener((obs, oldVal, newVal) -> {
            if (newVal) {
                fullScreenButtonOnTab1.setVisible(false);
                fullScreenButtonOffTab1.setVisible(true);
                fullScreenButtonOnTab2.setVisible(false);
                fullScreenButtonOffTab2.setVisible(true);
                fullScreenButtonOnTab3.setVisible(false);
                fullScreenButtonOffTab3.setVisible(true);
                fullScreenButtonOnTab4.setVisible(false);
                fullScreenButtonOffTab4.setVisible(true);
            } else {
                fullScreenButtonOnTab1.setVisible(true);
                fullScreenButtonOffTab1.setVisible(false);
                fullScreenButtonOnTab2.setVisible(true);
                fullScreenButtonOffTab2.setVisible(false);
                fullScreenButtonOnTab3.setVisible(true);
                fullScreenButtonOffTab3.setVisible(false);
                fullScreenButtonOnTab4.setVisible(true);
                fullScreenButtonOffTab4.setVisible(false);
            }
        });
    }

    @FXML
    private void handleFullScreenClickOff() {
        Stage primaryStage = (Stage) fullScreenButtonOffTab1.getScene().getWindow();
        primaryStage.setFullScreen(false);
        fullScreenButtonOnTab1.setVisible(true);
        fullScreenButtonOffTab1.setVisible(false);
        fullScreenButtonOnTab2.setVisible(true);
        fullScreenButtonOffTab2.setVisible(false);
        fullScreenButtonOnTab3.setVisible(true);
        fullScreenButtonOffTab3.setVisible(false);
        fullScreenButtonOnTab4.setVisible(true);
        fullScreenButtonOffTab4.setVisible(false);
    }

    @FXML
    private void handleParser2CleanButtonAction() {
        this.resetForm(parser2PreformattedTextArea, parser2PostformattedTextArea, parser2FormatButton, parser2CleanButton, parser2ResultsCopyButton);
    }

    @FXML
    private void handleParser2ResultsCopyButtonAction() {
        StringSelection stringSelection = new StringSelection(parser2PostformattedTextArea.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
    }

    @FXML
    private void handleParser2FormatButtonAction() {
        this.fillParser2PostformattedTextField();
    }

    /**
     * fill postformatted text area by template blocks
     */
    private void fillParser2PostformattedTextField() {
        String preformattedText = parser2PreformattedTextArea.getText();
        parser2PostformattedTextArea.setText(cleanText(preformattedText));

        String[] preformattedArray = preformattedText.split(POSTS_SEPARATOR);
        ArrayList<String> preformattedArrayList = new ArrayList<>(Arrays.asList(preformattedArray));
        StringBuilder postformattedString = new StringBuilder();

        preformattedArrayList.forEach(post -> {
            String postClean = post
                    .replaceAll(POSTS_SEPARATOR, "")
                    .replaceAll("(?m)^\\n+ *", "")
                    .replaceAll("(?m)^ +", "")
                    .trim();
            if (postClean.isEmpty()) return;

            String[] postLines = postClean.split("\\n");


            Pattern namePattern = Pattern
                    .compile("(?m)^\\[(.+?)\\]", Pattern.CASE_INSENSITIVE);
            Matcher nameMatcher = namePattern.matcher(postLines[0]);
            while (nameMatcher.find()) {
                String name = nameMatcher.group(1);
                // если нашли NAME
                if (name != null) {

                    postClean = postClean.replace("Description:",
                            parser2Text.getText() + " " +
                                    name + "\nDescription:");
                    postClean = postClean.replace("Category: ", "Category: " + name + ", ");
                    postformattedString.append(postClean).append("\n").append(POSTS_SEPARATOR).append("\n");
                }
            }
        });
        String output = postformattedString.toString();
        if (boldSubHeadersCheckbox.isSelected()) {
            output = output.replaceAll("(?m)(Category:|Related Categories:|Keywords:|Description:)", "[b]$0[/b]");
        }
        parser2PostformattedTextArea.setText(output);
    }
}
