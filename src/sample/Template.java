package sample;

public class Template {
    private String titleString;
    private String featuringString;
    private String tagsString;
    private Integer stringsInBlockCount;
    private Integer titleStringNumber;
    private Integer featuringStringNumber;
    private Integer tagsStringNumber;

    Template(String titleString, String featuringString, String tagsString) {
        this.titleString = titleString;
        this.featuringString = featuringString;
        this.tagsString = tagsString;
    }

    @Override
    public String toString() {
        return "sitename " + this.titleString + "\n" +
               "Featuring: " + this.featuringString + "\n" +
               "Tags: " + this.tagsString + "\n";
    }
}
